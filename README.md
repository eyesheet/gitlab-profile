# Eyesheet

Eyesheet is a web service for sharing and syncronising timers across devices. 
It is A FOSS and self-hostable and is implemented using a microservice
architecture.

## Running an instance

The app is not ready for prime time yet. It is possible to use the basic
functionalities however. An example for how to set up the services is provided
[here](https://gitlab.com/eyesheet/example-infrastructure) but doing your own
setup is recommended (if you do, please submit a PR or an issue to that repo
with a link so I can add it to the README).

## Team and contributions

The project is only developed by [me](https://tbjep.dk) (tbjep). If you want to
contribute or help in any way, please do not hesitate to submit issues or
PRs. Also feel free to reach out if you want to discuss the project but I can
sadly not help you setup an instance if you know nothing about
(Type|Java)Script, Rust, Nix, Docker, etc.